<?php
include "../lib/koneksi.php";
$idOrder = $_POST['id_order'];
$atasNama = $_POST['atas_nama'];
$transfer = strtolower($_POST['tipe_transfer']);
//gambar
$tmp_file = $_FILES['gambar']['tmp_name'];
$nama_file = $_FILES['gambar']['name'];
$ukuran_file = $_FILES['gambar']['size'];
$tipe_file = $_FILES['gambar']['type'];
$path = "../admin/upload/". $nama_file;
if (!($tipe_file == "image/jpeg" || $tipe_file == "image/png")){
  echo "<script>
          alert('Paket tidak dapat disimpan, gunakan gambar dengan ekstensi: JPG/JPEG/PNG');
          window.location=window.location='/order.php?page=konfirmasi&id_order='+'$idOrder';
        </script>";
  exit();
}

if ($ukuran_file > 1000000){
    echo "<script>
            alert('Paket tidak dapat disimpan, Ukuran Gambar melebihi 1MB');
            window.location=window.location='/order.php?page=konfirmasi&id_order='+'$idOrder';
          </script>";
      exit();
  }

if (!move_uploaded_file($tmp_file, $path)){
    echo "<script>
            alert('Paket tidak dapat disimpan, File gambar rusak atau server tidak dapat menyimpan file');
            window.location=window.location='/order.php?page=konfirmasi&id_order='+'$idOrder';
          </script>";
      exit();
  }

  if ($conn->query("insert into tbl_konfirmasi_order(id_order, atas_nama, tipe_transfer, screenshoot)
                values ('$idOrder', '$atasNama', '$transfer', '$nama_file')")
                === true){
                    $conn->query("update tbl_order set status='confirmed' where id_order='$idOrder'");
                    echo "<script>alert('Order telah dikonfirmasi');window.location='/';</script>";
}else{
  unlink($path);
  echo mysqli_error($conn);
    echo "<script>alert('Order tidak dapat dikonfirmasi atau telah dikonfirmasi atau id order tidak valid');window.location='/order.php?page=konfirmasi&id_order='+'$idOrder';</script>";
}
