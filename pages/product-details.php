<section>
		<div class="container">
			<div class="row">

			<?php
						include "template/inc/sidebar.php";
					?>
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="admin/upload/<?php echo $detailPaket['gambar']; ?>" alt="<?php echo $detailPaket['nama_paket']; ?>" />
								<h3>ZOOM</h3>
							</div>
							<!-- <div id="similar-product" class="carousel slide" data-ride="carousel">
								
								    <div class="carousel-inner">
										<div class="item active">
										  <a href=""><img src="asset/images/product-details/similar1.jpg" alt=""></a>
										  <a href=""><img src="asset/images/product-details/similar2.jpg" alt=""></a>
										  <a href=""><img src="asset/images/product-details/similar3.jpg" alt=""></a>
										</div>
										<div class="item">
										  <a href=""><img src="asset/images/product-details/similar1.jpg" alt=""></a>
										  <a href=""><img src="asset/images/product-details/similar2.jpg" alt=""></a>
										  <a href=""><img src="asset/images/product-details/similar3.jpg" alt=""></a>
										</div>
										<div class="item">
										  <a href=""><img src="asset/images/product-details/similar1.jpg" alt=""></a>
										  <a href=""><img src="asset/images/product-details/similar2.jpg" alt=""></a>
										  <a href=""><img src="asset/images/product-details/similar3.jpg" alt=""></a>
										</div>
										
									</div>

								  <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a>
							</div> -->

						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<img src="asset/images/product-details/new.jpg" class="newarrival" alt="" />
								<h2><?php echo $detailPaket['nama_paket']; ?></h2>
								<p>Web ID: <?php echo $detailPaket['id_paket']; ?></p>
								<img src="asset/images/product-details/rating.png" alt="" />
								<br>
								<span>
									<span>Rp. <?php echo $detailPaket['harga']; ?></span>
									<!-- <label>Quantity:</label>
									<input type="text" value="3" /> -->
									<?php if ($tersedia) {?>
									<a href="/order.php?id_paket=<?php echo $detailPaket['id_paket']; ?>" class="btn btn-fefault cart">
										<i class="fa fa-shopping-cart"></i>
										Pesan Paket
									</a>
									<?php }?>
								</span>
									<?php if (!$tersedia) {?><p>Opps, Paket ini tidak tersedia di lokasi anda.</p><?php }?>
								<!-- <p><b>Condition:</b> New</p> -->
								<p><b>Supplier:</b> <?php echo $detailPaket['nama']; ?></p>
								<a href=""><img src="asset/images/product-details/share.png" class="share img-responsive"  alt="" /></a>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="tab-content">
							<div class="tab-pane fade active in" id="reviews" >
								<div class="col-sm-12"><br>
								<h2 class="title text-center">Deskripsi Paket</h2>
									<p style="padding:0 20px;"><?php echo nl2br($detailPaket['deskripsi']); ?></p>
									<h2 class="title text-center">Menu Paket</h2>
									<?php $qmenu =  mysqli_query($conn, "select * from tbl_menu where id_paket='$idPaket'");
										while($menu = mysqli_fetch_array($qmenu)){?>	
									<div class="col-sm-3">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="admin/upload/<?php echo $menu['gambar']; ?>" alt="" />
													<p><b><?php echo $menu['nama_menu']; ?></b></p>
													<p><?php echo nl2br($menu['deskripsi']); ?></p>
												</div>
											</div>
										</div>
									</div>
										<?php }?>
								</div>
							</div>
							
						</div>
					</div><!--/category-tab-->
					
					<?php
						include "template/inc/recomended.php";
					?>
					
				</div>
			</div>
		</div>
	</section>
	