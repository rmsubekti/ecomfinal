<?php
require('../lib/fpdf/fpdf.php');
require('../lib/koneksi.php');
$qOrder = mysqli_query($conn, "select * from v_slip where id_order='$idOrder'");
$slip = mysqli_fetch_array($qOrder);
class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    //$this->Image('logo.png',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'Slip Pembayaran',0,0,'C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('Times','B',12);
$pdf->Cell(40,6,"ID Order ",0);
$pdf->Cell(2,6,": ",0);
$pdf->SetFont('Times','',12);
$pdf->Cell(100,6,$slip['id_order'],0);
$pdf->Ln();


$pdf->SetFont('Times','B',12);
$pdf->Cell(40,6,"Nama Penerima ",0);
$pdf->Cell(2,6,": ",0);
$pdf->SetFont('Times','',12);
$pdf->Cell(100,6,$slip['nama_pelanggan'],0);
$pdf->Ln();


$pdf->SetFont('Times','B',12);
$pdf->Cell(40,6,"Paket Makan",0);
$pdf->Cell(2,6,": ",0);
$pdf->SetFont('Times','',12);
$pdf->Cell(100,6,$slip['nama_paket'],0);
$pdf->Ln();


$pdf->SetFont('Times','B',12);
$pdf->Cell(40,6,"Alamat Penerima ",0);
$pdf->Cell(2,6,": ",0);
$pdf->SetFont('Times','',12);
$pdf->Cell(100,6,substr($slip['alamat'],0,78)."..",0);
$pdf->Ln();


$pdf->SetFont('Times','B',12);
$pdf->Cell(40,6,"Tanggal Mulai",0);
$pdf->Cell(2,6,": ",0);
$pdf->SetFont('Times','',12);
$pdf->Cell(100,6,$slip['tgl_order'],0);
$pdf->Ln();


$pdf->SetFont('Times','B',12);
$pdf->Cell(40,6,"Jam antar/terima",0);
$pdf->Cell(2,6,": ",0);
$pdf->SetFont('Times','',12);
$pdf->Cell(100,6,$slip['jam_order'],0);
$pdf->Ln();

$pdf->SetFont('Times','B',14);
$pdf->Cell(40,6,"Biaya ",0);
$pdf->Cell(2,6,": ",0);
$pdf->SetFont('Times','',14);
$pdf->Cell(100,6,"Rp. ".$slip['harga']*$slip['jumlah_hari']." ( Rp. ". $slip['harga']." X ".$slip['jumlah_hari']." hari )",0);
$pdf->Ln();


$pdf->Output();
?>