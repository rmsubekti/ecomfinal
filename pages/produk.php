<section id="advertisement">
		<div class="container">
			<img src="asset/images/shop/advertisement.jpg" alt="" />
		</div>
	</section>
	
	<section>
		<div class="container">
			<div class="row">
			<?php include "template/inc/sidebar.php"; ?>
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						<?php while($data= mysqli_fetch_array($result)){?>
							<div class="col-sm-4">
								<div class="product-image-wrapper">
									<div class="single-products">
											<div class="productinfo text-center">
												<img src="admin/upload/<?php echo $data['gambar']; ?>" alt="<?php echo $data['nama_paket']; ?>" />
												<h2>Rp. <?php echo $data['harga']; ?></h2>
												<p><?php echo $data['nama_paket']; ?></p>
												<a href="/product-details.php?id_paket=<?php echo $data['id_paket']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-th-large"></i>Lihat Menu</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2>Rp. <?php echo $data['harga']; ?></h2>
													<p><?php echo $data['nama_paket']; ?></p>
													<a href="/product-details.php?id_paket=<?php echo $data['id_paket']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-th-large"></i>Lihat Menu</a>
												</div>
											</div>
									</div>
									<!-- <div class="choose">
										<ul class="nav nav-pills nav-justified">
											<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
											<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
										</ul>
									</div> -->
								</div>
							</div>
						<?php }?>
					</div><!--features_items-->
						<ul class="pagination">
							<li class="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">&raquo;</a></li>
						</ul>
				</div>
			</div>
		</div>
	</section>