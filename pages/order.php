<?php
	$qpaket = mysqli_query($conn, "select * from tbl_paket where id_paket='$idPaket'");
	$paket = mysqli_fetch_array($qpaket);
?>
<section id="cart_items">
		<form action="pages/aksi_order.php" method="post">
		<input type="hidden" name="id_paket" value="<?php echo $idPaket;?>">
		<input type="hidden" name="id_member" value="<?php echo $_SESSION['id'];?>">
		<input type="hidden" name="alamat" value="<?php echo $_COOKIE['alamat'];?>">
		<input type="hidden" name="lat" value="<?php echo $_COOKIE['lat'];?>">
		<input type="hidden" name="lng" value="<?php echo $_COOKIE['lng'];?>">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Gambar</td>
							<td class="description">Nama Paket</td>
							<td class="price">Harga</td>
							<td class="quantity">Tanggal Mulai</td>
							<td class="total">Jumlah Hari</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cart_product">
								<a href=""><img width="60" src="admin/upload/<?php echo $paket['gambar']?>" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href=""><?php echo $paket['nama_paket']?></a></h4>
								<p>Web ID: <?php echo $paket['id_paket']?></p>
							</td>
							<td class="cart_price">
								<p>Rp. <?php echo $paket['harga']?></p>
							</td>
							<td class="cart_total">
							<input type="date" name="tgl_order"placeholder="12-30-2019"/>
							</td>
							<td class="cart_quantity">
								<input id="jmlHari" class="cart_quantity_input" type="number" name="jumlah_hari" value="1" min="1">
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>Apa yang ingin anda lakukan selanjutnya? </h3>
				<p>Tentukan waktu untuk menerima paket.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
								<br>
								<br>
						<ul class="user_option">
							<li>
								Waktu Paket akan diterima
								<br>
								<br>
							</li>
							<li>
								<input type="time" name="jam_order"/>
								<label>Mis. 06:30:PM</label>
							</li>
						</ul>
								<br>
								<br>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li>Total Harga <span id="total1">Rp. <?php echo $paket['harga']?></span></li>
							<li>Ongkos Kirim <span>Free</span></li>
							<li>Total <span id="total2">Rp. <?php echo $paket['harga']?></span></li>
						</ul>
							<!-- <a class="btn btn-default update" href="">Update</a> -->
							<button type="submit" class="btn btn-default check_out" href="">Check Out</button>
					</div>
				</div>
			</div>
		</div>
		</form>
	</section><!--/#do_action-->

	<script>
	var jmlHari = document.getElementById('jmlHari');
	jmlHari.addEventListener("change", getTotal);
	function getTotal() {
		document.getElementById("total1").innerHTML = "Rp. "+(<?php echo $paket['harga']?> * jmlHari.value);
		document.getElementById("total2").innerHTML = "Rp. "+(<?php echo $paket['harga']?> * jmlHari.value);
	}
	</script>
