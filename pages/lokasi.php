<section>
		<div class="container">
			<div class="row">
			    <?php include "template/inc/sidebar.php"; ?>
				<div class="col-sm-9 padding-right">
						<div class="tab-content">
							<div class="tab-pane fade active in" id="reviews" >
								<div class="col-sm-12">
									<p><b>Tentukan Lokasi anda</b> untuk melihat paket yang tersedia di dekat lokasi anda</p>
                                    <?php if(isset($idPaket)){?>
									<form action="product-details.php" method="post">
                                        <input type="hidden" name="id_paket" value="<?php echo $idPaket ?>">
                                    <?php } else {?> <form action="/" method="post"> <?php }?>
										<span>
											<input id="alamatMember" name="lokasi" type="text" placeholder="Alamat "/>
                                            <button type="submit" class="btn btn-default pull-right">
                                                Submit
                                            </button>
										</span>
									</form>
								</div>
							</div>
						</div>
                </div>
            </div>
        </div>
</section>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBn0MziGHhmz4a0yAMaOIjYcUdUEEdBzEU&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    var placeSearch, autocomplete/*, geocoder*/;
    function initAutocomplete() {
        // geocoder = new google.maps.Geocoder();
        placeSearch = document.getElementById('alamatMember');
        
        autocomplete = new google.maps.places.Autocomplete(
            (placeSearch),{componentRestrictions: {country: 'id'}}
        );
        autocomplete.addListener('place_changed', fillInAddress);
    }

    // function codeAddress(address) {
    //     geocoder.geocode( { 'address': address}, function(results, status) {
    //         if (status == 'OK') {
    //             console.log(results[0].geometry.location.lng());
    //         } else {
    //             alert('Geocode was not successful for the following reason: ' + status);
    //         }
    //     });
    // }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        //console.log(place);

        // if (!place.place_id) {
        //     return;
        // }
        
        var d = new Date();
        d.setTime(d.getTime() + (1*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = "alamat=" + placeSearch.value + ";" + expires + ";path=/";
        document.cookie = "lat=" + place.geometry.location.lat() + ";" + expires + ";path=/";
        document.cookie = "lng=" + place.geometry.location.lng() + ";" + expires + ";path=/";
    }
</script>