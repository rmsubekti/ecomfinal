<section id="slider"><!--slider-->
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div id="slider-carousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
						<li data-target="#slider-carousel" data-slide-to="1"></li>
						<li data-target="#slider-carousel" data-slide-to="2"></li>
					</ol>
					
					<div class="carousel-inner">
						<div class="item active">
							<div class="col-sm-6">
								<h1><span>E</span>-SHOPPER</h1>
								<h2>Free E-Commerce Template</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
								<button type="button" class="btn btn-default get">Get it now</button>
							</div>
							<div class="col-sm-6">
								<img src="asset/images/home/girl1.jpg" class="girl img-responsive" alt="" />
								<img src="asset/images/home/pricing.png"  class="pricing" alt="" />
							</div>
						</div>
						<div class="item">
							<div class="col-sm-6">
								<h1><span>E</span>-SHOPPER</h1>
								<h2>100% Responsive Design</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
								<button type="button" class="btn btn-default get">Get it now</button>
							</div>
							<div class="col-sm-6">
								<img src="asset/images/home/girl2.jpg" class="girl img-responsive" alt="" />
								<img src="asset/images/home/pricing.png"  class="pricing" alt="" />
							</div>
						</div>
						
						<div class="item">
							<div class="col-sm-6">
								<h1><span>E</span>-SHOPPER</h1>
								<h2>Free Ecommerce Template</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
								<button type="button" class="btn btn-default get">Get it now</button>
							</div>
							<div class="col-sm-6">
								<img src="asset/images/home/girl3.jpg" class="girl img-responsive" alt="" />
								<img src="asset/images/home/pricing.png" class="pricing" alt="" />
							</div>
						</div>
						
					</div>
					
					<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>
					<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
				
			</div>
		</div>
	</div>
</section><!--/slider-->

<section>
	<div class="container">
		<div class="row">
			<?php include "template/inc/sidebar.php"; ?>
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					<h2 class="title text-center">Paket Terbaru</h2>
					<?php $fpaket = mysqli_query($conn, "select * from tbl_paket order by id_paket desc limit 6");
						while($pkt = mysqli_fetch_array($fpaket)){?>
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
									<div class="productinfo text-center">
										<img src="admin/upload/<?php echo $pkt['gambar']; ?>" alt="<?php echo $pkt['nama_paket']; ?>" />
										<h2>Rp. <?php echo $pkt['harga']; ?></h2>
										<p><?php echo $pkt['nama_paket']; ?></p>
										<a href="/product-details.php?id_paket=<?php echo $pkt['id_paket']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-th-large"></i>Lihat Menu</a>
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2>Rp. <?php echo $pkt['harga']; ?></h2>
											<p><?php echo $pkt['nama_paket']; ?></p>
											<a href="/product-details.php?id_paket=<?php echo $pkt['id_paket']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-th-large"></i>Lihat Menu</a>
										</div>
									</div>
							</div>
							<!-- <div class="choose">
								<ul class="nav nav-pills nav-justified">
									<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
									<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
								</ul>
							</div> -->
						</div>
					</div>
					<?php }?>
					
				</div><!--features_items-->
				
				<div class="category-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
						<?php $tkategori = mysqli_query($conn, "select * from tbl_kategori order by id_kategori asc limit 3 ");
							$firstkat = true;
						while($kat = mysqli_fetch_array($tkategori)){?>
							<li class="<?php if($firstkat) echo "active"; ?>">
								<a href="#kat<?php echo $kat['id_kategori'];?>" data-toggle="tab">
									<?php echo $kat['nama'];?>
								</a>
							</li>
							<?php 
							$firstkat=false; 
							}?>
						</ul>
					</div>
					<div class="tab-content">
						<?php $pkategori = mysqli_query($conn, "select * from tbl_kategori order by id_kategori asc limit 3 ");
							$ktgf = true;
						 while($skat = mysqli_fetch_array($pkategori)){?>
						<div class="tab-pane fade <?php if($ktgf) echo 'active in'?>" id="kat<?php echo $skat['id_kategori'];?>" >
						<?php 
							$idKategori = $skat['id_kategori'];
							$kpaket = mysqli_query($conn, "select * from tbl_paket where id_kategori='$idKategori' order by id_paket desc limit 4") 
							or die( mysqli_error($conn));
							while($kpkt = mysqli_fetch_array($kpaket)){?>
							<div class="col-sm-3">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<img src="admin/upload/<?php echo $kpkt['gambar']; ?>" alt="" />
											<h2>Rp. <?php echo $kpkt['harga']; ?></h2>
											<p><?php echo $kpkt['nama_paket']; ?></p>
											<a href="/product-details.php?id_paket=<?php echo $kpkt['id_paket']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-th-large"></i>Lihat Menu</a>
										</div>
									</div>
								</div>
							</div>
							<?php }?>
						</div>
						<?php $ktgf = false; }?>
					</div>
				</div><!--/category-tab-->
				<?php include "template/inc/recomended.php";?>
			</div>
		</div>
	</div>
</section>
