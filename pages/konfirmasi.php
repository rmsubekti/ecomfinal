<?php if(isset($_GET['id_order'])){?>
	<section id="do_action">
		<div class="container">
			<div class="heading col-sm-offset-3">
				<h3>Konfirmasikan Order</h3>
				<p>Silakan masukkan detail pembayaran untuk mengkonfirmasi order</p>
			</div>
			<form enctype="multipart/form-data" action="pages/aksi_konfirmasi.php" method="post">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								ID Order : #<?php echo $_GET['id_order'];?><input name="id_order" type="hidden" size="10" value="<?php echo $_GET['id_order'];?>"/>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Tipe Transfer:</label>
								<select name="tipe_transfer">
									<option>Bank</option>
									<option>Paypal</option>
									<option>Gopay</option>
								</select>
							</li>
							<li class="single_field zip-field">
								<label>Atas Nama:</label>
								<input type="text" name="atas_nama">
							</li>
							<li class="single_field">
								<label>Bukti Pembayaran:</label>
								<input type="file" id="gambar" name="gambar">
							</li>
						</ul>
						<button type="submit" class="btn btn-default update" href="">Konfirmasi</button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</section><!--/#do_action-->
<?php } else {?>
	<section style="margin:50px 0" id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="login-form"><!--login form-->
						<h2>ID Order yang akan dikonfirmasi :</h2>
						<form action="order.php" method="get">
							<input type="hidden" name="page" value="konfirmasi" />
							<input type="text" name="id_order" placeholder="ID Order" />
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
<?php } ?>