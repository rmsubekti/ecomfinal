<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form action="admin/login.php" method="post">
							<input name="username" type="text" placeholder="Username" />
							<input name="password"type="password" placeholder="Password" />
							<!-- <span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span> -->
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						<form action="admin/aksi_register.php" method="post">
							<input type="hidden" id="lok_lon" name="lok_lon">
							<input type="hidden" id="lok_lat" name="lok_lat">
							<input name="member" type="hidden" value="pelanggan"/>
							<input name="nama" type="text" placeholder="Nama"/>
							<input name="username" type="text" placeholder="Username"/>
							<input name="email" type="email" placeholder="Email Address"/>
							<input name="password" type="password" placeholder="Password"/>
							<input id="alamat" name="alamat" type="text" placeholder="Alamat"/>
							<input name="nohp" type="text" placeholder="Nomor Telpon"/>
							<button type="submit" class="btn btn-default">Signup</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBn0MziGHhmz4a0yAMaOIjYcUdUEEdBzEU&libraries=places&callback=initAutocomplete" async defer></script>
<script>var placeSearch, autocomplete/*, geocoder*/;

    var lok_lng= document.getElementById('lok_lon');
    var lok_lat= document.getElementById('lok_lat');

    function initAutocomplete() {
        // geocoder = new google.maps.Geocoder();
        placeSearch = document.getElementById('alamat');
        // if (placeSearch.value === "") {
        //     return;
        // }
        autocomplete = new google.maps.places.Autocomplete(
            (placeSearch),{componentRestrictions: {country: 'id'}}
        );

        autocomplete.addListener('place_changed', fillInAddress);
    }

    // function codeAddress(address) {
    //     geocoder.geocode( { 'address': address}, function(results, status) {
    //         if (status == 'OK') {
    //             console.log(results[0].geometry.location.lng());
    //         } else {
    //             alert('Geocode was not successful for the following reason: ' + status);
    //         }
    //     });
    // }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        console.log(place);

        if (!place.place_id) {
            return;
        }
        lok_lng.value = place.geometry.location.lng();
        lok_lat.value = place.geometry.location.lat();
        //alert(place.place_id);
        // codeAddress(document.getElementById('alamatSupplier').value);
    }</script>