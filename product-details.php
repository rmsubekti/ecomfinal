<?php
    include "template/header.php";
    $idPaket = $_GET['id_paket'] ? $_GET['id_paket'] : $_POST['id_paket'];
    $dPaket = mysqli_query($conn, "select * from v_paket_detail where id_paket='$idPaket'");
    $detailPaket = mysqli_fetch_array($dPaket);
    $tersedia = true;
    
    if(isset($_COOKIE['lat']) and isset($_COOKIE['lng'])) {
        $lat1 = $detailPaket['lokasi_latitude'];
        $lon1 = $detailPaket['lokasi_longitude'];
        $lat2 = $_COOKIE['lat'];
        $lon2 = $_COOKIE['lng'];
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            include "pages/product-details.php";
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $approx = $miles * 1.609344; // Kilometer
            if($approx <= 4 ){
                $tersedia = true;
            }else{
                $tersedia = false;
            } 
        }

        include "pages/product-details.php";
    } else include "pages/lokasi.php";
    include "template/footer.php";
?>
