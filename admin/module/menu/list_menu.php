<?php
$idPaket = $_GET['id_paket'];
$query = "select * from tbl_menu where id_paket='$idPaket'";
$result = $conn->query($query);
?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Menu
                        <small>
                        <?php if ($_GET['module']=='tambah_menu') echo "Menu Yang telah ditambahkan"; else echo "Semua Menu"?>
                        </small>
                    </h3>
                    <?php if ($_GET['module']=='edit_paket') {?>
                    <!-- tools box -->
                    <div class="pull-right box-tools">
                        <a class="btn btn-danger pull-right" href="adminweb.php?module=tambah_menu&id_paket=<?php echo $idPaket;?>">
                            <i class="fa fa-power-off"></i> Tambah Menu
                        </a>
                    </div>
                    <?php }?>
                    <!-- /. tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <?php if ($result->num_rows>0) {?>
                        <table class="table table-hover">
                            <tr>
                                <th>Menu</th>
                                <th>Gambar</th>
                                <th>Aksi</th>
                            </tr>
                            <?php
                            while($row = $result->fetch_assoc()){?>
                                <tr>
                                    <td><?php echo $row['nama_menu']?></td>
                                    <td><img src="upload/<?php 
                                    if (file_exists('upload/'.$row['gambar'])){
                                        echo $row['gambar'];
                                    }else{
                                        echo 'no_image.svg';
                                    }
                                    ?>"
                                             width="150"
                                             alt="<?php echo $row['nama_menu'] ?>"></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-warning" href="<?php echo $admin_url; ?>adminweb.php?module=edit_menu&id_menu=<?php echo $row['id_menu']; ?>">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a class="btn btn-danger" href="<?php echo $admin_url; ?>module/menu/aksi_hapus.php?id_menu=<?php echo $row['id_menu']; ?>&id_paket=<?php echo $row['id_paket']; ?>">
                                                <i class="fa fa-power-off"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } else {?>
                        <p>Tidak ada data untuk ditampilkan</p>
                    <?php }?>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->