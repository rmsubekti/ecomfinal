<?php
session_start();

include "../../../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
    echo "<script>
            alert('anda harus login untuk mengakses module');
            window.location='$admin_url';
          </script>";
    exit();
}
if ($_SESSION['role'] !== 'supplier'){
    echo "<script>
            alert('anda tidak dapat mengubah menu sebagai admin');
            window.location='$admin_url'+'adminweb.php?module=home';
          </script>";
    exit();
}
include "../../../lib/koneksi.php";
$idPaket = $_GET['id_paket'];
$idMenu = $_GET['id_menu'];
$deleteMenu = "delete from tbl_menu where id_menu='$idMenu'";
if ($conn->query($deleteMenu) === true){
echo "<script>alert('data menu berhasil dihapus');window.location='$admin_url'+'adminweb.php?module=edit_paket&id_paket='+'$idPaket';</script>";
}
else {
    echo "<script>alert('data menu tidak dapat dihapus');window.location='$admin_url'+'adminweb.php?module=edit_paket&id_paket='+'$idPaket';</script>";
}
    