<?php
session_start();
include "../../../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
    echo "<script>
            alert('anda harus login untuk mengakses module');
            window.location='$admin_url';
          </script>";
    exit();
}
if ($_SESSION['role'] !== 'supplier'){
    echo "<script>
            alert('anda tidak dapat menambah menu sebagai admin');
            window.location='$admin_url'+'adminweb.php?module=home';
          </script>";
    exit();
}

include "../../../lib/koneksi.php";

$idPaket = $_POST['idPaket'];
//gambar
$tmp_file = $_FILES['gambar']['tmp_name'];
$nama_file = $_FILES['gambar']['name'];
$ukuran_file = $_FILES['gambar']['size'];
$tipe_file = $_FILES['gambar']['type'];
$path = "../../upload/". $nama_file;
if (!($tipe_file == "image/jpeg" || $tipe_file == "image/png")){
  echo "<script>
          alert('Menu tidak dapat disimpan, gunakan gambar dengan ekstensi: JPG/JPEG/PNG');
          window.location='$admin_url'+'adminweb.php?module=tambah_menu&id_paket=$idPaket';
        </script>";
  exit();
}

if ($ukuran_file > 1000000){
    echo "<script>
            alert('Menu tidak dapat disimpan, Ukuran Gambar melebihi 1MB');
            window.location='$admin_url'+'adminweb.php?module=tambah_menu&id_paket=$idPaket';
          </script>";
      exit();
  }

if (!move_uploaded_file($tmp_file, $path)){
    echo "<script>
            alert('Menu tidak dapat disimpan, File gambar rusak atau server tidak dapat menyimpan file');
            window.location='$admin_url'+'adminweb.php?module=tambah_menu&id_paket=$idPaket';
          </script>";
      exit();
  }

//lain
$namaMenu = $_POST['namaMenu'];
$deskripsiMenu = $_POST['deskripsi'];

if ($conn->query(
    "insert into tbl_menu(id_paket, nama_menu, deskripsi,gambar )
                values ('$idPaket', '$namaMenu', '$deskripsiMenu','$nama_file')")
                === true){
                    echo "<script>alert('data menu berhasil disimpan');window.location='$admin_url'+'adminweb.php?module=tambah_menu&id_paket=$idPaket';</script>";
}else{
  unlink($path);
  echo mysqli_error($conn);
    echo "<script>alert('data menu tidak dapat disimpan');window.location='$admin_url'+'adminweb.php?module=tambah_menu&id_paket=$idPaket';</script>";
}
