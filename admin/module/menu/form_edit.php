<?php
$idMenu = $_GET['id_menu'];
$qMenu = $conn->query("select * from tbl_menu where id_menu='$idMenu'");
$menu = mysqli_fetch_array($qMenu);
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Menu
                        <small>Edit Info Menu</small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <form enctype="multipart/form-data" action="../admin/module/menu/aksi_edit.php" method="POST" class="form-horizontal">
                      <input type="hidden" name="idMenu" value="<?php echo $idMenu; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="namaMenu" class="col-sm-2 control-label">Nama Menu</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $menu['nama_menu'] ?>" placeholder="Nama Menu" id="namaMenu" name="namaMenu">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gambar" class="col-sm-2 control-label">Gambar</label>
                                <div class="col-sm-10">
                                    <img width="200" src="upload/<?php 
                                    if (file_exists('upload/'.$menu['gambar'])){
                                        echo $menu['gambar'];
                                    }else{
                                        echo 'no_image.svg';
                                    }
                                    ?>">
                                    <input type="file" id="gambar" name="gambar">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsiMenu" class="col-sm-2 control-label">Deskripsi Menu</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $menu['deskripsi'] ?>" placeholder="Deskripsi Menu" id="deskripsiMenu" name="deskripsiMenu">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->
</section>
<!-- /.content -->