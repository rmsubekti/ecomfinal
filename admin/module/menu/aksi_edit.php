<?php
session_start();
include "../../../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
  echo "<script>
          alert('anda harus login untuk mengakses module');
          window.location='$admin_url';
        </script>";
  exit();
}
if ($_SESSION['role'] !== 'supplier'){
  echo "<script>
          alert('anda tidak dapat mengubah menu sebagai admin');
          window.location='$admin_url'+'adminweb.php?module=home';
        </script>";
  exit();
}

include "../../../lib/koneksi.php";
//gambar
$tmp_file = $_FILES['gambar']['tmp_name'];
$nama_file = $_FILES['gambar']['name'];
$ukuran_file = $_FILES['gambar']['size'];
$tipe_file = $_FILES['gambar']['type'];
$path = "../../upload/". $nama_file;

//check auth
$idMenu = $_POST['idMenu'];
$qmenu = mysqli_fetch_array($conn->query("select * from tbl_menu where id_menu='$idMenu'"));
$idPaket = $qmenu['id_paket'];
//lain
$namaMenu = $_POST['namaMenu'];
$deskripsiMenu = $_POST['deskripsiMenu'];


if (getimagesize($tmp_file) !== false) {
  if (!($tipe_file == "image/jpeg" || $tipe_file == "image/png")){
    echo "<script>
            alert('Menu tidak dapat diupdate, gunakan gambar dengan ekstensi: JPG/JPEG/PNG');
            window.location='$admin_url'+'adminweb.php?module=edit_menu&id_menu=$idMenu';
          </script>";
    exit();
  }

  if ($ukuran_file > 1000000){
      echo "<script>
              alert('Menu tidak dapat diupdate, Ukuran Gambar melebihi 1MB');
              window.location='$admin_url'+'adminweb.php?module=edit_menu&id_menu=$idMenu';
            </script>";
        exit();
    }

  if (!move_uploaded_file($tmp_file, $path)){
      echo "<script>
              alert('Menu tidak dapat diupdate, File gambar rusak atau server tidak dapat menyimpan file');
              window.location='$admin_url'+'adminweb.php?module=edit_menu&id_menu=$idMenu';
            </script>";
        exit();
    }
}else {
  $nama_file = $qmenu['gambar'];
}

if ($conn->query(
    "update tbl_menu set nama_menu='$namaMenu', deskripsi='$deskripsiMenu',  gambar='$nama_file' where id_menu='$idMenu'")
                === true){
        if ($nama_file !== $qmenu['gambar']){ unlink("../../upload/". $qmenu['gambar']);}
        echo "<script>alert('data menu berhasil diupdate');window.location='$admin_url'+'adminweb.php?module=edit_paket&id_paket=$idPaket';</script>";
}
else{
  unlink($path);
  echo mysqli_error($conn);
    echo "<script>alert('data menu tidak dapat diupdate');window.location='$admin_url'+'adminweb.php?module=edit_menu&id_menu=$idMenu';</script>";
}
