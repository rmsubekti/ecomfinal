<?php
$idPaket = $_GET['id_paket'];
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Menu
                        <small>Tambah Menu</small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <form enctype="multipart/form-data" action="../admin/module/menu/aksi_simpan.php" method="POST" class="form-horizontal">
                        <input type="hidden" id="idPaket" name="idPaket" value="<?php echo $idPaket; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="namaMenu" class="col-sm-2 control-label">Nama Menu</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Nama Menu" id="namaMenu" name="namaMenu">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gambar" class="col-sm-2 control-label">Gambar</label>
                                <div class="col-sm-10">
                                    <input type="file" id="gambar" name="gambar">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsiMenu" class="col-sm-2 control-label">Deskripsi Menu</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Deskripsi Menu" id="deskripsiMenu" name="deskripsi">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a class="btn btn-default" href="/admin/adminweb.php?module=edit_paket&id_paket=<?php echo $idPaket; ?>">Selesai</a>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
<?php 
    include 'module/menu/list_menu.php';?>
    <!-- ./row -->
</section>
<!-- /.content -->