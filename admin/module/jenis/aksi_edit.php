<?php
session_start();

include "../../../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
    echo "<script>
            alert('anda harus login untuk mengakses module');
            window.location='$admin_url';
          </script>";
}else if($_SESSION['role'] !== 'supplier'){
            echo "<script>
                    alert('anda tidak dapat mengedit jenis sebagai admin');
                    window.location='$admin_url';
                  </script>";
}else{
    include "../../../lib/koneksi.php";

    $idJenis = $_POST['id_jenis'];
    $namaJenis = $_POST['nama_jenis'];
    $query = "update tbl_jenis set nama='$namaJenis' where id_jenis='$idJenis'";
    if ($conn->query($query) === true){
        echo "<script>alert('data jenis berhasil diupdate');window.location='$admin_url'+'adminweb.php?module=jenis';</script>";
    }
    else{
        echo "<script>alert('data jenis tidak dapat diupdate');window.location='$admin_url'+'adminweb.php?module=edit_jenis';</script>";
    }
}