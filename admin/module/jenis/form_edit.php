<?php
$idJenis = $_GET['id_jenis'];
$query = "select * from tbl_jenis where id_jenis='$idJenis'";
$result = mysqli_query($conn, $query);
$data = mysqli_fetch_array($result);
$namaJenis = $data['nama'];
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Jenis
                        <small>Edit Jenis</small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <form method="post"action="../admin/module/jenis/aksi_edit.php" class="form-horizontal">
                        <input type="hidden" name="id_jenis" value="<?php echo $idJenis?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="namaJenis" class="col-sm-2 control-label">Nama Jenis</label>
                                <div class="col-sm-10">
                                    <input type="text" value="<?php echo $namaJenis?>" name="nama_jenis" id="namaJenis" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->
</section>
<!-- /.content -->