<?php
$idPaket = $_GET['id_paket'];
$qPaket = $conn->query("select * from tbl_paket where id_paket='$idPaket'");
$paket = mysqli_fetch_array($qPaket);
if (($_SESSION['role'] !== 'supplier') and ($_SESSION['id'] !== $paket['id_member'])){
    echo "<script>
            alert('anda tidak dapat merubah paket yang bukan milik anda');
            window.location='$admin_url';
          </script>";
    exit();
}

$qkategori = $conn->query("select * from tbl_kategori");
$qjenis = $conn->query("select * from tbl_jenis");
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Paket
                        <small>Edit Info Paket</small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <form enctype="multipart/form-data" action="../admin/module/paket/aksi_edit.php" method="POST" class="form-horizontal">
                      <input type="hidden" name="idPaket" value="<?php echo $idPaket; ?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="idKategori" class="col-sm-2 control-label">Kategori</label>
                                <div class="col-sm-10">
                                    <select name="idKategori" id="idKategori" class="form-control">
                                        <?php while ($kat = $qkategori->fetch_assoc()){?>
                                        <option <?php if ($kat['id_kategori'] === $paket['id_kategori']) echo "selected"; ?> value="<?php echo $kat['id_kategori'] ?>"><?php echo $kat['nama'] ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="idJenis" class="col-sm-2 control-label">Jenis Makanan</label>
                                <div class="col-sm-10">
                                    <select name="idJenis" id="idJenis" class="form-control">
                                        <?php while ($mer = $qjenis->fetch_assoc()){?>
                                            <option <?php if ($mer['id_jenis'] === $paket['id_jenis']) echo "selected"; ?> value="<?php echo $mer['id_jenis'] ?>"><?php echo $mer['nama'] ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="namaPaket" class="col-sm-2 control-label">Nama Paket</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $paket['nama_paket'] ?>" placeholder="Nama Paket" id="namaPaket" name="namaPaket">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gambar" class="col-sm-2 control-label">Gambar</label>
                                <div class="col-sm-10">
                                    <img width="200" src="upload/<?php 
                                    if (file_exists('upload/'.$paket['gambar'])){
                                        echo $paket['gambar'];
                                    }else{
                                        echo 'no_image.svg';
                                    }
                                    ?>">
                                    <input type="file" id="gambar" name="gambar">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsiPaket" class="col-sm-2 control-label">Deskripsi Paket</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $paket['deskripsi'] ?>" placeholder="Deskripsi Paket" id="deskripsiPaket" name="deskripsiPaket">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hargaPaket" class="col-sm-2 control-label">Harga Paket</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" value="<?php echo $paket['harga'] ?>" placeholder="Harga Paket" id="hargaPaket" name="hargaPaket">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->
<?php 
    include 'module/menu/list_menu.php';?>
</section>
<!-- /.content -->