<?php
session_start();
include "../../../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
    echo "<script>
            alert('anda harus login untuk mengakses module');
            window.location='$admin_url';
          </script>";
    exit();
}
if ($_SESSION['role'] !== 'supplier'){
    echo "<script>
            alert('anda tidak dapat menambah paket sebagai admin');
            window.location='$admin_url';
          </script>";
    exit();
}

include "../../../lib/koneksi.php";
//gambar
$tmp_file = $_FILES['gambar']['tmp_name'];
$nama_file = $_FILES['gambar']['name'];
$ukuran_file = $_FILES['gambar']['size'];
$tipe_file = $_FILES['gambar']['type'];
$path = "../../upload/". $nama_file;
if (!($tipe_file == "image/jpeg" || $tipe_file == "image/png")){
  echo "<script>
          alert('Paket tidak dapat disimpan, gunakan gambar dengan ekstensi: JPG/JPEG/PNG');
          window.location='$admin_url'+'adminweb.php?module=tambah_paket';
        </script>";
  exit();
}

if ($ukuran_file > 1000000){
    echo "<script>
            alert('Paket tidak dapat disimpan, Ukuran Gambar melebihi 1MB');
            window.location='$admin_url'+'adminweb.php?module=tambah_paket';
          </script>";
      exit();
  }

if (!move_uploaded_file($tmp_file, $path)){
    echo "<script>
            alert('Paket tidak dapat disimpan, File gambar rusak atau server tidak dapat menyimpan file');
            window.location='$admin_url'+'adminweb.php?module=tambah_paket';
          </script>";
      exit();
  }

//lain
$idKategori = $_POST['idKategori'];
$idJenis= $_POST['idJenis'];
$namaPaket = $_POST['namaPaket'];
$deskripsiPaket = $_POST['deskripsiPaket'];
$hargaPaket = $_POST['hargaPaket'];
$idMember = $_SESSION['id'];


if ($conn->query(
    "insert into tbl_paket(id_kategori, id_member, id_jenis, nama_paket, deskripsi, harga, gambar )
                values ('$idKategori', '$idMember', '$idJenis', '$namaPaket', '$deskripsiPaket','$hargaPaket','$nama_file')")
                === true){
                    $qId =$conn->query("SELECT LAST_INSERT_ID() as current")->fetch_assoc();
                    $idPaket = $qId['current'];
                    echo "<script>alert('data paket berhasil disimpan, selanjutnya anda perlu menambahkan menu');window.location='$admin_url'+'adminweb.php?module=tambah_menu&id_paket='+'$idPaket';</script>";
}else{
  unlink($path);
  echo mysqli_error($conn);
    echo "<script>alert('data paket tidak dapat disimpan');window.location='$admin_url'+'adminweb.php?module=tambah_paket';</script>";
}
