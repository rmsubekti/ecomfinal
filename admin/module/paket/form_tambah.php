<?php
$qkategori = $conn->query("select * from tbl_kategori");
$qjenis = $conn->query("select * from tbl_jenis");
?>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Paket
                        <small>Tambah Paket</small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <form enctype="multipart/form-data" action="../admin/module/paket/aksi_simpan.php" method="POST" class="form-horizontal">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="idKategori" class="col-sm-2 control-label">Kategori</label>
                                <div class="col-sm-10">
                                    <select name="idKategori" id="idKategori" class="form-control">
                                        <?php while ($kat = $qkategori->fetch_assoc()){?>
                                        <option value="<?php echo $kat['id_kategori'] ?>"><?php echo $kat['nama'] ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="idJenis" class="col-sm-2 control-label">Jenis Makanan</label>
                                <div class="col-sm-10">
                                    <select name="idJenis" id="idJenis" class="form-control">
                                        <?php while ($jen = $qjenis->fetch_assoc()){?>
                                            <option value="<?php echo $jen['id_jenis'] ?>"><?php echo $jen['nama'] ?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="namaPaket" class="col-sm-2 control-label">Nama Paket</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Nama Paket" id="namaPaket" name="namaPaket">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="gambar" class="col-sm-2 control-label">Gambar</label>
                                <div class="col-sm-10">
                                    <input type="file" id="gambar" name="gambar">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsiPaket" class="col-sm-2 control-label">Deskripsi Paket</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Deskripsi Paket" id="deskripsiPaket" name="deskripsiPaket">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hargaPaket" class="col-sm-2 control-label">Harga Paket</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Harga Paket" id="hargaPaket" name="hargaPaket">
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->
</section>
<!-- /.content -->