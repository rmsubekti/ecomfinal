<?php
session_start();
include "../../../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
    echo "<script>
            alert('anda harus login untuk mengakses module');
            window.location='$admin_url';
          </script>";
}

include "../../../lib/koneksi.php";
//gambar
$tmp_file = $_FILES['gambar']['tmp_name'];
$nama_file = $_FILES['gambar']['name'];
$ukuran_file = $_FILES['gambar']['size'];
$tipe_file = $_FILES['gambar']['type'];
$path = "../../upload/". $nama_file;

//check auth
$idPaket = $_POST['idPaket'];
$qpaket = mysqli_fetch_array($conn->query("select * from tbl_paket where id_paket='$idPaket'"));
if (($_SESSION['role'] !== 'supplier') and ($_SESSION['id'] !== $qpaket['id_member'])){
    echo "<script>
            alert('anda tidak dapat merubah paket yang bukan milik anda');
            window.location='$admin_url';
          </script>";
    exit();
}
//lain
$idKategori = $_POST['idKategori'];
$idJenis= $_POST['idJenis'];
$namaPaket = $_POST['namaPaket'];
$deskripsiPaket = $_POST['deskripsiPaket'];
$hargaPaket = $_POST['hargaPaket'];


if (getimagesize($tmp_file) !== false) {
  if (!($tipe_file == "image/jpeg" || $tipe_file == "image/png")){
    echo "<script>
            alert('Paket tidak dapat diupdate, gunakan gambar dengan ekstensi: JPG/JPEG/PNG');
            window.location='$admin_url'+'adminweb.php?module=edit_paket&id_paket=$idPaket';
          </script>";
    exit();
  }

  if ($ukuran_file > 1000000){
      echo "<script>
              alert('Paket tidak dapat diupdate, Ukuran Gambar melebihi 1MB');
              window.location='$admin_url'+'adminweb.php?module=edit_paket&id_paket=$idPaket';
            </script>";
        exit();
    }

  if (!move_uploaded_file($tmp_file, $path)){
      echo "<script>
              alert('Paket tidak dapat diupdate, File gambar rusak atau server tidak dapat menyimpan file');
              window.location='$admin_url'+'adminweb.php?module=edit_paket&id_paket=$idPaket';
            </script>";
        exit();
    }
}else {
  $nama_file = $qpaket['gambar'];
}

if ($conn->query(
    "update tbl_paket set id_kategori='$idKategori', id_jenis='$idJenis', nama_paket='$namaPaket', deskripsi='$deskripsiPaket',
    harga='$hargaPaket', gambar='$nama_file' where id_paket='$idPaket'")
                === true){
        if ($nama_file !== $qpaket['gambar']) unlink("../../upload/". $qpaket['gambar']);
        echo "<script>alert('data paket berhasil diupdate');window.location='$admin_url'+'adminweb.php?module=paket';</script>";
}
else{
  unlink($path);
  echo mysqli_error($conn);
    echo "<script>alert('data paket tidak dapat diupdate');window.location='$admin_url'+'adminweb.php?module=edit_paket&id_paket=$idPaket';</script>";
}
