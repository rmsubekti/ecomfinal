<?php
session_start();

include "../../../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
    echo "<script>
            alert('anda harus login untuk mengakses module');
            window.location='$admin_url';
          </script>";
}else if($_SESSION['role'] !== 'supplier'){
    echo "<script>
            alert('anda tidak dapat membuat kategori sebagai admin');
            window.location='$admin_url';
          </script>";
}else{
    include "../../../lib/koneksi.php";

    $namaKategori = $_POST['namaKategori'];
    $simpan = $conn->query("insert into tbl_kategori(nama) values ('$namaKategori')");
    if ( $simpan === true){
        echo "<script>alert('data kategori berhasil disimpan');window.location='$admin_url'+'adminweb.php?module=kategori';</script>";
    }
    else{
        //echo mysqli_error($conn);
        echo "<script>alert('data kategori tidak dapat disimpan');window.location='$admin_url'+'adminweb.php?module=tambah_kategori';</script>";
    }
}