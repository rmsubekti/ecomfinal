<?php
session_start();

include "../../../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
    echo "<script>
            alert('anda harus login untuk mengakses module');
            window.location='$admin_url';
          </script>";
}else if($_SESSION['role'] !== 'supplier'){
            echo "<script>
                    alert('anda tidak dapat menghapus kategori sebagai admin');
                    window.location='$admin_url';
                  </script>";
}else{
    include "../../../lib/koneksi.php";

    $idKategori = $_GET['id_kategori'];
    $query = "delete from tbl_kategori where id_kategori='$idKategori'";
    if ($conn->query($query) === true){
        echo "<script>alert('data kategori berhasil dihapus');window.location='$admin_url'+'adminweb.php?module=kategori';</script>";
    }
    else{
        echo "<script>alert('data kategori tidak dapat dihapus');window.location='$admin_url'+'adminweb.php?module=kategori';</script>";
    }
}