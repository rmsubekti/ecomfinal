<?php
$idOrder = $_GET['id_order'];
$query = "select * from v_supplier_detail_order where id_order='$idOrder'";
$result = mysqli_query($conn, $query);
$data = mysqli_fetch_array($result);
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Order
                        <small>Detail Order</small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <div class="form-horizontal">
                        <input type="hidden" name="id_order" value="<?php echo $idOrder?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="namaPelanggan" class="col-sm-2 control-label">Nama Pelanggan</label>
                                <div class="col-sm-10">
                                    <p id="namaPelanggan" class="form-control"> <?php echo $data['nama_member'];?> </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="namaPaket" class="col-sm-2 control-label">Nama Paket</label>
                                <div class="col-sm-10">
                                    <p id="namaPaket" class="form-control"> <?php echo $data['nama_paket'];?> </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jumlahHari" class="col-sm-2 control-label">Jumlah Hari Pemesanan</label>
                                <div class="col-sm-10">
                                    <p id="jumlahHari" class="form-control"> 
                                        <?php echo $data['tgl_order']." s/d ".$data['tgl_order_sd']." (".$data['jumlah_hari']." hari) ";?> 
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jam" class="col-sm-2 control-label">Diantar Setiap Pukul</label>
                                <div class="col-sm-10">
                                    <p id="jam" class="form-control"> 
                                        <?php echo $data['jam_order'];?> 
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-sm-2 control-label">Alamat </label>
                                <div class="col-sm-10">
                                    <p id="alamat" class="form-control"> 
                                        <?php echo $data['alamat'];?> 
                                        <a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php echo $data['lokasi_latitude'].','.$data['lokasi_longitude'] ?>">Lihat di Map</a>
                                    </p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->
</section>
<!-- /.content -->