<?php
$idMember = $_SESSION['id'];
$query = ($_SESSION['role'] == 'admin') ? "select * from v_order where status='confirmed'" : "select * from v_order where status='validated' and id_member='$idMember'";
$result = $conn->query($query);
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Order
                        <small>Semua Order</small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <?php if ($result->num_rows>0) {?>
                        <table class="table table-hover">
                            <tr>
                                <th>ID Order</th>
                                <th>Tanggal Order</th>
                                <th>Nama Member </th>
                                <th>Nama Paket</th>
                                <th>Aksi</th>
                            </tr>
                            <?php
                            while($row = $result->fetch_assoc()){?>
                                <tr>
                                    <td><?php echo $row['id_order']?></td>
                                    <td><?php echo $row['tgl_order']?></td>
                                    <td><?php echo $row['nama_member']?></td>
                                    <td><?php echo $row['nama_paket']?></td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-warning" href="<?php echo $admin_url; ?>adminweb.php?module=detail_order&id_order=<?php echo $row['id_order']; ?>">
                                                Lihat Detail
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } else {?>
                        <p>Tidak ada data untuk ditampilkan</p>
                    <?php }?>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->
</section>
<!-- /.content -->