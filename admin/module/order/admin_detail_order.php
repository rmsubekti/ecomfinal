<?php
$idOrder = $_GET['id_order'];
$query = "select * from v_admin_detail_order where id_order='$idOrder'";
$result = mysqli_query($conn, $query);
$data = mysqli_fetch_array($result);

?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Order
                        <small>Detail Order</small>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body pad">
                    <form method="post"action="../admin/module/order/validasi_order.php" class="form-horizontal">
                        <input type="hidden" name="id_order" value="<?php echo $data['id_order'];?>">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="namaPelanggan" class="col-sm-2 control-label">Nama Pelanggan</label>
                                <div class="col-sm-10">
                                    <p id="namaPelanggan" class="form-control"> <?php echo $data['nama_member'];?> </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="namaPaket" class="col-sm-2 control-label">Nama Paket</label>
                                <div class="col-sm-10">
                                    <p id="namaPaket" class="form-control"> <?php echo $data['nama_paket'];?> </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hargaPaket" class="col-sm-2 control-label">Jumlah Transfer</label>
                                <div class="col-sm-10">
                                    <p id="hargaPaket" class="form-control"> <?php echo $data['harga']*$data['jumlah_hari'];?> </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="jumlahHari" class="col-sm-2 control-label">Jumlah Hari Pemesanan</label>
                                <div class="col-sm-10">
                                    <p id="jumlahHari" class="form-control"> 
                                        <?php echo $data['tgl_order']." s/d ".$data['tgl_order_sd']." (".$data['jumlah_hari']." hari) ";?> 
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="detailPembayaran" class="col-sm-2 control-label">Pembayaran</label>
                                <div class="col-sm-10">
                                    <p id="detailPembayaran" class="form-control"> 
                                        <?php echo "Transfer <b>".$data['tipe_transfer']."</b> atas nama <b>".$data['atas_nama']."</b>";?> 
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="buktiPembayaran" class="col-sm-2 control-label">Bukti Pembayaran</label>
                                <div class="col-sm-10">
                                    <p id="buktiPembayaran"> 
                                    <img width="600" src="upload/<?php 
                                    if (file_exists('upload/'.$data['screenshoot'])){
                                        echo $data['screenshoot'];
                                    }else{
                                        echo 'no_image.svg';
                                    }
                                    ?>">
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                        <?php echo mysqli_error($conn);?>
                            <button type="submit" class="btn btn-primary pull-right">Validasi</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-->
    </div>
    <!-- ./row -->
</section>
<!-- /.content -->