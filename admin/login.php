<?php
/**
 * Created by PhpStorm.
 * User: su
 * Date: 23/03/19
 * Time: 20:03
 */
include "../lib/koneksi.php";

$user = $_POST['username'];
$pass = $_POST['password'];

if (!ctype_alnum($user) OR !ctype_alnum($pass)) {
    echo "Login gagal, Username dan password tidak boleh kosong";
    echo "<a href='index.php'>Ulangi</a>";
    //echo "$user : $pass";
    //header('location:index.php');
}else {
    //$login = $connec->query("select * from tbl_admin where username='$user' and password='$pass'");
    $query =  "SELECT * FROM tbl_member WHERE username='$user' AND password='$pass'";
    $result = mysqli_query($conn, $query);
    //echo "$query";
    if (!$result){
        echo "query err";
    }
    if (mysqli_num_rows($result)>0) {
        $log = mysqli_fetch_array($result);
        session_start();
        $_SESSION['id'] = $log['id'];
        $_SESSION['role'] = $log['role'];
        $_SESSION['nama'] = $log['nama'];
        setcookie('alamat', $log['alamat'], time() + (86400 * 30), "/"); // 86400 = 1 day
        setcookie('lat', $log['lokasi_latitude'], time() + (86400 * 30), "/"); // 86400 = 1 day
        setcookie('lng', $log['lokasi_longitude'], time() + (86400 * 30), "/"); // 86400 = 1 day
        if ($log['role'] == 'pelanggan') {
            header('location:/');
        }
        else header('location:adminweb.php?module=home');
    }else {
        echo "Login gagal, Kombinasi Username dan password tidak cocok";
        echo "<a href='index.php'>Ulangi</a>";
    }
}
