<?php
/**
 * Created by PhpStorm.
 * User: su
 * Date: 23/03/19
 * Time: 20:12
 */?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="asset/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?php if ($_GET['module']=='home') echo 'active'; ?>">
                <a href="adminweb.php?module=home">
                    <i class="fa fa-home"></i> <span>Home</span>
                </a>
            </li>

            <li class="<?php if ($_GET['module']=='order') echo 'active'; ?>">
                <a href="adminweb.php?module=order">
                    <i class="fa fa-bar-chart"></i> <span>Order</span>
                    <span class="pull-right-container">
                          <?php
                            $idme = $_SESSION['id'];
                            $sq = ($_SESSION['role'] == 'admin') ? "select count(id_order) as jml from v_order where status='confirmed'" : "select count(id_order) as jml from v_order where status='validated' and id_member='$idme'";
                            $qJml = mysqli_fetch_array(mysqli_query($conn, $sq));
                            if ($qJml['jml'] > 0) {
                          ?>
                      <small class="label pull-right bg-red">
                          <?php  echo $qJml['jml'];  ?>
                      </small>
                            <?php }?>
                    </span>
                </a>
            </li>

            <?php // Menu untuk supplier
            if ($_SESSION['role'] == 'supplier'){?>
            <li class="<?php if ($_GET['module']=='kategori') echo 'active'; ?>">
                <a href="adminweb.php?module=kategori">
                    <i class="fa fa-bars"></i>
                    <span>Kategori</span>
                </a>
            </li>
            <li class="<?php if ($_GET['module']=='jenis') echo 'active'; ?>">
                <a href="adminweb.php?module=jenis">
                    <i class="fa fa-eye"></i> <span>Jenis</span>
                </a>
            </li>
            <li class="<?php if ($_GET['module']=='paket') echo 'active'; ?>">
                <a href="adminweb.php?module=paket">
                    <i class="fa fa-th"></i>
                    <span>Paket</span>
                </a>
            </li>
            <?php }?>

            <?php // Menu untuk admin
            if ($_SESSION['role'] == 'admin'){?>
            <li>
                <a href="adminweb.php?module=member&role=supplier">
                    <i class="fa fa-car"></i> <span>Supplier</span>
                </a>
            </li>
            <li>
                <a href="adminweb.php?module=member&role=pelanggan">
                    <i class="fa fa-users"></i> <span>Pengguna</span>
                </a>
            </li>
            <li>
                <a href="adminweb.php?module=member&role=admin">
                    <i class="fa fa-cog"></i> <span>Admin</span>
                </a>
            </li>
            <?php }?>
            <li>
                <a href="logout.php">
                    <i class="fa fa-power-off"></i> <span>Logout</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
