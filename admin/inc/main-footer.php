<?php
/**
 * Created by PhpStorm.
 * User: su
 * Date: 23/03/19
 * Time: 20:18
 */
?>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
</footer>

  <!-- Control Sidebar -->
  <?php include 'control-sidebar.php';?>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="asset/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="asset/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="asset/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="asset/dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="asset/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="asset/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="asset/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="asset/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="asset/bower_components/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="asset/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="asset/dist/js/demo.js"></script>

<?php if ($_GET['module']=='tambah_supplier' || $_GET['module']=='edit_supplier') {?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBn0MziGHhmz4a0yAMaOIjYcUdUEEdBzEU&libraries=places&callback=initAutocomplete" async defer></script>
<script>var placeSearch, autocomplete/*, geocoder*/;

    var lok_lng= document.getElementById('lok_lng');
    var lok_lat= document.getElementById('lok_lat');

    function initAutocomplete() {
        // geocoder = new google.maps.Geocoder();
        placeSearch = document.getElementById('alamatSupplier');
        // if (placeSearch.value === "") {
        //     return;
        // }
        autocomplete = new google.maps.places.Autocomplete(
            (placeSearch),{componentRestrictions: {country: 'id'}}
        );
        
        autocomplete.addListener('place_changed', fillInAddress);
    }

    // function codeAddress(address) {
    //     geocoder.geocode( { 'address': address}, function(results, status) {
    //         if (status == 'OK') {
    //             console.log(results[0].geometry.location.lng());
    //         } else {
    //             alert('Geocode was not successful for the following reason: ' + status);
    //         }
    //     });
    // }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        console.log(place);
        
        if (!place.place_id) {
            return;
        }
        lok_lng.value = place.geometry.location.lng();
        lok_lat.value = place.geometry.location.lat();
        //alert(place.place_id);
        // codeAddress(document.getElementById('alamatSupplier').value);
    }</script>
<?php }?>
</body>
</html>