<?php
/**
 * Created by PhpStorm.
 * User: su
 * Date: 23/03/19
 * Time: 20:14
 */
session_start();

include "../lib/config.php";
if (empty($_SESSION['id']) and empty($_SESSION['role'])){
    echo "<script>
            alert('anda harus login untuk mengakses halaman admin');
            window.location='$admin_url';
          </script>";
}
else if ($_SESSION['role']=='pelanggan') {
    echo "<script>
            alert('anda tidak dapat mengakses halaman ini sebagai pelanggan');
            window.location='$base_url';
          </script>";
}
include "../lib/koneksi.php";
include 'inc/main-header.php';
?>

  <!-- Left side column. contains the logo and sidebar -->
    <?php include 'inc/main-sidebar.php';?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
<?php
//kategori
if ($_GET['module']=='kategori') {
    include 'module/kategori/list_kategori.php';
}else if ($_GET['module']=='tambah_kategori') {
    include 'module/kategori/form_tambah.php';
}else if ($_GET['module']=='edit_kategori') {
    include 'module/kategori/form_edit.php';
}else

// Jenis
if ($_GET['module']=='jenis') {
    include 'module/jenis/list_jenis.php';
}else if ($_GET['module']=='tambah_jenis') {
    include 'module/jenis/form_tambah.php';
}else if ($_GET['module']=='edit_jenis') {
    include 'module/jenis/form_edit.php';
}else

// Order
if ($_GET['module']=='order') {
    include 'module/order/list_order.php';
}else if ($_GET['module']=='detail_order') {
    if ($_SESSION['role']=='admin') include 'module/order/admin_detail_order.php';
    else include 'module/order/supplier_detail_order.php';
}else 

// Member
if ($_GET['module']=='member') {
    include 'module/member/list_member.php';
}else 

// Menu
if ($_GET['module']=='menu') {
    include 'module/menu/list_menu.php';
}else if ($_GET['module']=='tambah_menu') {
    include 'module/menu/form_tambah.php';
}else if ($_GET['module']=='edit_menu') {
    include 'module/menu/form_edit.php';
}else

//paket
if ($_GET['module']=='paket') {
    include 'module/paket/list_paket.php';
}else if ($_GET['module']=='tambah_paket') {
    include 'module/paket/form_tambah.php';
}else if ($_GET['module']=='edit_paket') {
    include 'module/paket/form_edit.php';
}else {
    include 'module/home/home.php';
}
 ?>

  </div>
  <!-- /.content-wrapper -->

    <?php include 'inc/main-footer.php';?>
