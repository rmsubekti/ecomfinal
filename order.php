<?php
    include "template/header.php";
    $idPaket = $_GET['id_paket'];
    
	if (empty($_SESSION['id']) and empty($_SESSION['role'])){
		echo "<script>
				alert('Woopps, anda harus login untuk melakukan order');
            </script>";
        include "pages/login.php";
    } else if(isset($_GET['page'])){
        include "pages/konfirmasi.php";
    }else include "pages/order.php";
    include "template/footer.php";
?>
