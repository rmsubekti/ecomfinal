<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <style>
        #autocomplete {
            width: 100%;
        }
    </style>
    <title>JS Bin</title>
</head>
<body>
<div id="locationField">
    <input id="autocomplete" placeholder="Enter your address" type="text" />
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA2HOdi81bV-MiJOvZHDbTZA4JBGprD4gs&libraries=places&callback=initAutocomplete" async defer></script>
<script>var placeSearch, autocomplete/*, geocoder*/;

    function initAutocomplete() {
        // geocoder = new google.maps.Geocoder();
        placeSearch = document.getElementById('autocomplete');
        autocomplete = new google.maps.places.Autocomplete(
            (placeSearch),{componentRestrictions: {country: 'id'}}
        );
        if (placeSearch.value === "") {
            return;
        }
        
        autocomplete.addListener('place_changed', fillInAddress);
    }

    // function codeAddress(address) {
    //     geocoder.geocode( { 'address': address}, function(results, status) {
    //         if (status == 'OK') {
    //             console.log(results[0].geometry.location.lng());
    //         } else {
    //             console.log(status);
    //             alert('Geocode was not successful for the following reason: ' + status);
    //         }
    //     });
    // }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        
        if (!place.place_id) {
            return;
        }
        console.log(place.geometry.location.lng());
        //codeAddress(document.getElementById('autocomplete').value);
    }</script>
</body>
</html>