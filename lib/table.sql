create table tbl_member(
  id integer PRIMARY KEY auto_increment ,
  username varchar(50) not null unique,
  password varchar(50) not null,
  role ENUM('admin','supplier','pelanggan') default 'pelanggan',
  nama varchar(50) not null,
  alamat varchar(250),
  lokasi_longitude varchar(50),
  lokasi_latitude varchar(50),
  email varchar(50) not null,
  no_hp varchar(50) not null
);

create table tbl_kategori(
  id_kategori integer PRIMARY KEY auto_increment,
  nama varchar(50) unique
);

create table tbl_jenis(
  id_jenis integer PRIMARY KEY auto_increment,
  nama varchar(50) unique
);

create table tbl_paket(
  id_paket integer PRIMARY KEY auto_increment,
  id_kategori integer not null,
  id_member integer not null,
  id_jenis integer not null,
  nama_paket varchar(50) unique,
  gambar varchar(100),
  deskripsi text,
  harga integer not null,
  foreign key(id_kategori) references tbl_kategori(id_kategori),
  foreign key(id_jenis) references tbl_jenis(id_jenis),
  foreign key(id_member) references tbl_member(id)
);

create table tbl_menu(
  id_menu integer PRIMARY KEY auto_increment,
  id_paket integer not null,
  nama_menu varchar(100) not null,
  deskripsi text,
  gambar varchar(100),
  foreign key(id_paket) references tbl_paket(id_paket)
);

create table tbl_order(
  id_order integer PRIMARY KEY auto_increment,
  id_paket integer not null,
  id_member integer not null,
  jumlah_hari integer not null,
  tgl_order date not null,
  jam_order time not null,
  alamat varchar(250) not null,
  status ENUM('unconfirmed','confirmed','validated') default 'unconfirmed',
  lokasi_longitude varchar(50),
  lokasi_latitude varchar(50),
  foreign key(id_paket) references tbl_paket(id_paket),
  foreign key(id_member) references tbl_member(id)
);

create table tbl_konfirmasi_order(
  id_order integer not null unique,
  atas_nama varchar(50),
  tipe_transfer ENUM('bank','paypal','gopay') not null,
  screenshoot varchar(100) not null,
  foreign key(id_order) references tbl_order(id_order)
);
