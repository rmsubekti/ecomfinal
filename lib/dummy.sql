-- Admin
insert into tbl_member (username,password,role,nama,alamat,lokasi_longitude,lokasi_latitude,email,no_hp) 
    values ('admin','admin','admin','nama admin','Unnamed Road, Dero, Condongcatur, Depok Sub-District, Sleman Regency, Special Region of Yogyakarta 55281',
    '110.4119351','-7.7541596','email@dot.com','00000');

--Supplier
insert into tbl_member (username,password,role,nama,alamat,lokasi_longitude,lokasi_latitude, email, no_hp) 
    values ('tanto','tanto','supplier','Kopi Menoreh Kang Tanto','Dusun Cikalan, Boku, Banjarharjo, Kalibawang, Kulon Progo Regency, Special Region of Yogyakarta 55672',
            '110.2468818','-7.6968001','email@dot.com','0000'),
           ('masjan','masjan','supplier','Lesehan Masjan Bakar & Goreng','Jl. Nusa Indah, Dero, Condongcatur, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55283',
            '110.4087925','-7.7554959','email@dot.com','0000'),
           ('sasanti','sasanti','supplier','Sasanti Restaurant','Jl. Palagan No.52A, Panggung Sari, Sariharjo, Ngaglik, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55582',
            '110.3599658','-7.745603','email@dot.com','0000');

-- Pelanggan
insert into tbl_member (username,password,nama,alamat,lokasi_longitude,lokasi_latitude, email, no_hp) 
    values ('amikom','amikom','University of AMIKOM Yogyakarta','Jl. Ringroad Utara, Sumberan, Sariharjo, Ngaglik, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55581',
            '110.4087079','-7.7600779','email@dot.com','0000'),
           ('larissa','larissa','Larissa Craft','Jl. Jengger No.15B, Jongkang, Sariharjo, Ngaglik, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55581',
            '110.373307','-7.7458631','email@dot.com','0000'),
           ('anto','anto','ANTO CEL groups','Jl. Nanggulan Mendut, Ngemplak, Banjarharjo, Kalibawang, Kabupaten Kulon Progo, Daerah Istimewa Yogyakarta 55672',
            '110.2530415','-7.6960703','email@dot.com','0000');

-- Jenis
insert into tbl_jenis(nama) values ('Chinese'), ('Indian'), ('Indonesian'), ('Japanese'), ('Javanese') ;

-- Kategori
insert into tbl_kategori(nama) values ('Makan Malam'), ('Sarapan'), ('Makan Pagi');

-- Paket

-- Menu