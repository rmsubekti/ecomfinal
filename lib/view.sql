CREATE VIEW v_order AS
select o.id_order, o.tgl_order, adddate(o.tgl_order, interval o.jumlah_hari day) as tgl_berlaku_order,
o.status, p.id_member, p.nama_paket,p.harga, m.nama as nama_member
from tbl_order o join tbl_paket p on o.id_paket= p.id_paket
join tbl_member m on o.id_member=m.id where adddate(o.tgl_order, interval o.jumlah_hari day)  >= CURRENT_DATE();

CREATE VIEW v_admin_detail_order AS
select o.id_order, o.tgl_order,o.jumlah_hari, adddate(o.tgl_order, interval o.jumlah_hari day) as tgl_order_sd,
p.nama_paket,p.harga, 
m.nama as nama_member,
k.screenshoot, k.atas_nama, k.tipe_transfer
from tbl_order o left join tbl_paket p on o.id_paket= p.id_paket
left join tbl_member m on o.id_member=m.id 
left join tbl_konfirmasi_order k on k.id_order = o.id_order;
 
CREATE VIEW v_supplier_detail_order AS
select o.id_order, o.tgl_order,o.jumlah_hari,o.jam_order, o.lokasi_longitude, o.lokasi_latitude,o.alamat,
adddate(o.tgl_order, interval o.jumlah_hari day) as tgl_order_sd,
p.nama_paket, p.id_member,
m.nama as nama_member
from tbl_order o join tbl_paket p on o.id_paket= p.id_paket
join tbl_member m on o.id_member=m.id;

create view v_jenis as 
select count(p.id_jenis) as jumlah, j.id_jenis, j.nama from tbl_jenis j left join tbl_paket p on 
j.id_jenis = p.id_jenis GROUP by j.id_jenis;

CREATE VIEW v_paket_terlaris as
select count(o.id_paket) as jumlah,p.id_paket, p.nama_paket, p.harga, p.gambar from tbl_order o right join tbl_paket p
on o.id_paket = p.id_paket GROUP by p.id_paket;

CREATE VIEW v_paket_detail as
select count(o.id_paket) as jumlah,p.id_paket, p.nama_paket, p.harga, p.gambar, p.deskripsi, m.id, m.nama, m.alamat,
m.lokasi_longitude,m.lokasi_latitude
from tbl_order o right join tbl_paket p on o.id_paket = p.id_paket 
join tbl_member m on m.id = p.id_member
GROUP by p.id_paket;

create view v_slip as
select o.id_order, o.jumlah_hari, o.tgl_order, o.jam_order, o.alamat, m.nama as nama_pelanggan, p.nama_paket, p.harga 
from tbl_order o join tbl_member m on o.id_member = m.id join tbl_paket p on o.id_paket = p.id_paket

create view v_kategori_produk as
select k.nama as nama_kategori ,p.*,m.lokasi_longitude, m.lokasi_latitude 
from tbl_kategori k join tbl_paket p on k.id_kategori = p.id_kategori
join tbl_member m on p.id_member=m.id

create view v_jenis_produk as
select j.nama as nama_jenis ,p.*,m.lokasi_longitude, m.lokasi_latitude 
from tbl_jenis j join tbl_paket p on j.id_jenis = p.id_jenis
join tbl_member m on p.id_member=m.id

create view v_all_produk as
select p.*,m.lokasi_longitude, m.lokasi_latitude 
from tbl_paket p join tbl_member m on p.id_member=m.id