<?php
/**
 * Created by PhpStorm.
 * User: su
 * Date: 23/03/19
 * Time: 19:56
 */
$dbusername = 'root';
$dbpassword = 'secretpassword';
$dbserver = '127.0.0.1';
$dbname='ecomfinal';

//docker run --name ecomfinal  -e MYSQL_ROOT_PASSWORD=secretpassword -e MYSQL_DATABASE=ecomfinal -p 3306:3306 -d mysql --default-authentication-plugin=mysql_native_password

$conn = mysqli_connect($dbserver, $dbusername, $dbpassword, $dbname);

if( !$conn ) // == null if creation of connection object failed
{
$conn = mysqli_connect( "127.0.0.1", $dbusername, $dbpassword, $dbname )

    // report the error to the user, then exit program
    or die ("connection object not created: ".mysqli_error($conn));
}

if( mysqli_connect_errno() )  // returns false if no error occurred
{
    // report the error to the user, then exit program
    echo ("Connect failed: ".mysqli_connect_errno()." : ". mysqli_connect_error());
}
