<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<?php $kategori = mysqli_query($conn, "select * from tbl_kategori");
							while($kat = mysqli_fetch_array($kategori)){?>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="/produk.php?kategori=<?php echo $kat['id_kategori'];?>"><?php echo $kat['nama'];?></a></h4>
								</div>
							</div>
							<?php }?>
						</div><!--/category-products-->
					
						<div class="brands_products"><!--brands_products-->
							<h2>Jenis Makanan</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<?php $jenis = mysqli_query($conn, "select * from v_jenis");
									while($jen = mysqli_fetch_array($jenis)){?>
									<li><a href="/produk.php?jenis=<?php echo $jen['id_jenis']; ?>"> <span class="pull-right">(<?php echo $jen['jumlah']; ?>)</span><?php echo $jen['nama'];?></a></li>
									<?php }?>
								</ul>
							</div>
						</div><!--/brands_products-->
						
						<div class="price-range"><!--price-range-->
							<h2>Price Range</h2>
							<div class="well text-center">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
								 <b class="pull-left">$ 0</b> <b class="pull-right">$ 600</b>
							</div>
						</div><!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<img src="asset/images/home/shipping.jpg" alt="" />
						</div><!--/shipping-->
					
					</div>
				</div>
				