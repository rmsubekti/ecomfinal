<div class="recommended_items"><!--recommended_items-->
	<h2 class="title text-center">Paket Terlaris</h2>
	
	<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="item active">
               <?php $lpaket = mysqli_query($conn, "select * from v_paket_terlaris order by jumlah desc limit 6;");
                  $tpa = 0;
                  while($lpak = mysqli_fetch_array($lpaket)){?>	
				<div class="col-sm-4">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<img src="admin/upload/<?php echo $lpak['gambar'];?>" alt="<?php echo $lpak['nama_paket'];?>" />
								<h2>Rp. <?php echo $lpak['harga'];?></h2>
								<p><?php echo $lpak['nama_paket'];?></p>
								<a href="/product-details.php?id_paket=<?php echo $lpak['id_paket']; ?>" class="btn btn-default add-to-cart"><i class="fa fa-th-large"></i>Lihat Menu</a>
							</div>
							
						</div>
					</div>
                </div>
                <?php if (++$tpa == 3 ) {?>
			</div>
			<div class="item">
                
            <?php } }?>
            </div>
		</div>
		 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
			<i class="fa fa-angle-left"></i>
		  </a>
		  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
			<i class="fa fa-angle-right"></i>
		  </a>			
	</div>
</div><!--/recommended_items-->
					